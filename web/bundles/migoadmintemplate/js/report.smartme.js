
var dataDatatableConfig = {
        "dom": 'lfBrtip',
        "buttons": [
            {
                "extend": 'print',
                "text": '<span class="fa fa-print"></span>',
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ],
                },
                "title": 'Listado de Panelistas',
                "titleAttr": 'IMPRIMIR'
            },
            {
                "extend": 'excelHtml5',
                "text": '<span class="fa fa-file-excel-o"></span>',
                "orientation": "landscape",
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                },
                "title": 'Listado de Panelistas',
                "titleAttr": 'EXCEL'
            },
            {
                "extend": 'pdfHtml5',
                "text": '<span class="fa fa-file-pdf-o"></span>',
                "orientation": "landscape",
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                },
                "title": 'Listado de Panelistas',
                "titleAttr": 'PDF'
            },
            {
                "text": '<span class="glyphicon glyphicon-bullhorn"></span>',
                "className": 'push-masive-button',
                "titleAttr": 'Envío de Push/SMS Masivo'
            }
        ],
        "columnDefs": [
            {
                "title": "<input type=\"checkbox\" class=\"panelist_checkbox\" id=\"panelist_checkbox\" value=\"1\">",
                "targets": 0,
                "className": 'text-center panelist_all',
                "orderable": false
            }
        ],
        "ajax": {
            "url": "/reporte/smartme/consult",
            "type": "POST",
            "data": function ( d ) {
                d.estado = $("#estado").val();
                d.codigo_invitacion = $("#codigo_invitacion").val();
                d.fecha_registro = $("#fecha_registro").val();
                d.fecha_sincronizacion = $("#fecha_sincronizacion").val();
                d.puntos = $("#puntos").val();
            },
        },
        "columns": [
            {
                "data": null,
                "render": function ( data, type, row ) {
                  return '<input class="panelist_checkbox" type="checkbox" name="panelists[]" value="'+row.id_panelista+'">';
                },
                "defaultContent": "N/A"
            },
            {
                "data":"nombre",
                "defaultContent": "N/A"
            },
            {
                "data":"email",
                "defaultContent": "N/A"
            },
            {
                "data": null,
                "render": function ( data, type, row ) {
                    if(row.fecha_nacimiento){
                        return '<span style="display: none;">'+convertDateNumber(row.fecha_nacimiento)+'</span>'+convertDateFormat(row.fecha_nacimiento);
                    }
                },
                "defaultContent": "N/A"
            },
            {
                "data":"codigo_postal",
                "defaultContent": "N/A"
            },
            {
                "data": null,
                "render": function ( data, type, row ) {
                    if(row.estado == 1){
                        return "Activo";
                    }
                    else{
                        return "Inactivo";
                    }
                },
                "defaultContent": "N/A"
            },
            {
                "data": null,
                "render": function ( data, type, row ) {
                    if(row.create_at){
                        return '<span style="display: none;">'+convertDateNumber(row.create_at)+'</span>'+convertDateFormat(row.create_at);
                    }
                },
                "defaultContent": "N/A"
            },
            {
                "data":"codigo_invitacion",
                "defaultContent": "N/A"
            },
            {
                "data": null,
                "render": function ( data, type, row ) {
                    if(row.ultima_sincronizacion){
                        return '<span style="display: none;">'+convertDateNumber(row.ultima_sincronizacion)+'</span>'+convertDateFormat(row.ultima_sincronizacion);
                    }
                },
                "defaultContent": "N/A"
            },
            {
                "data": "racha",
                "defaultContent": "N/A"
            },
            {
                "data":"puntos",
                "defaultContent": "N/A"
            },
            {
                "data":"porcentaje_actividad",
                "defaultContent": "N/A"
            },
            {
                "data": null,
                "render": function ( data, type, row ) {
                    var botones = '<a href="#" data-toggle="modal" data-target="#modal-panelis-send-push" title="Enviar notificación Push/SMS" class="panelist-sent-push" data-panelist-id="'+row.id_panelista+'" data-panelist-name="'+row.nombre+'">';
                        botones+= '<i class="fa fa-mobile"></i></a>';
                        botones+= '<a href="/panelista/history/point/'+row.id_panelista+'" style="margin-left: 4px;" title="Historial de Puntos">';
                        botones+= '<i class="fa fa-list-ol"></i></a>';
                    return botones;
                },
                "defaultContent": "N/A"
            },
        ],
        "bProcessing": true
};

function listSmartme (){
    $('#panelist_list').dataTable()._fnAjaxUpdate();
}

$('body')
    .on('click', '#button-filtrar', function (event) {
        event.preventDefault();
        listSmartme();
    })
    .on('click', '#button-limpiar', function (event) {
        event.preventDefault();
        $("#estado").val('');
        $("#codigo_invitacion").val('');
        $("#fecha_registro").val('');
        $("#fecha_sincronizacion").val($("#fecha-hoy").html() + ' - ' + $("#fecha-hoy").html());
        $("#puntos").val('');
        $('#fecha_registro_button span').html('<i class="fa fa-calendar"></i> Seleccione');
        $('#fecha_sincronizacion_button span').html('<i class="fa fa-calendar"></i> Hoy');
        listSmartme();
    })
;