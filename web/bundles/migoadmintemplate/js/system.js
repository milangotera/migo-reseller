$(window).load(function() {
    $(".loader").fadeOut("slow");
});

(function($){
    var dataTableDefaultOptions = {
       "paging": true,
       "lengthChange": true,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ Elementos",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "No se encontraron resultados",
            "sInfo":           "Mostrando _START_ a _END_ de _TOTAL_",
            "sInfoEmpty":      "Mostrando 0 al 0 de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    };
    if (typeof dataDatatableConfig != "undefined"){
        Object.assign( dataTableDefaultOptions, dataDatatableConfig );
    }
    $(function(){

        $('table.table-datatable').DataTable(dataTableDefaultOptions);

        $('body')
            .on('click', 'td.row-action', function (event) {
                event.preventDefault();
                var $td = $(this), url = $td.parent().data('row-action');
                document.location.replace(url);
            })
        });
    
})(jQuery);


$(document).ready(function () {
    
    function Reloj(){
        var tiempo = new Date();
        var hora = tiempo.getHours();
        var minuto = tiempo.getMinutes();
        var segundo = tiempo.getSeconds();
        var dia = tiempo.getDate();
        var mes = tiempo.getMonth() +1;
        var anio = tiempo.getFullYear();
        str_hora = new String(hora);
        if (str_hora.length == 1) {
            hora = '0'+hora;
        }
        str_minuto = new String(minuto);
        if (str_minuto.length == 1) {
            minuto = '0'+minuto;
        }
        str_segundo = new String(segundo);
        if (str_segundo.length == 1) {
            segundo = '0'+segundo;
        }
        str_dia = new String(dia);
        if (str_dia.length == 1) {
            dia = '0'+dia;
        }
        str_mes = new String(mes);
        if (str_mes.length == 1) {
            mes = '0'+mes;
        }
        $('#actual_fecha').text(dia+'/'+mes+'/'+anio);
        $('#actual_hora').text(hora+':'+minuto+':'+segundo);
    }

    setInterval(Reloj, 1000);

});
