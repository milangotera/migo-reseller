
var dataDatatableConfig = {
        "dom": 'lfBrtip',
        "buttons": [
            {
                "extend": 'print',
                "text": '<span class="fa fa-print"></span>',
                "title": 'Historial de Puntos: '+$('#panelista-name').html(),
                "titleAttr": 'IMPRIMIR'
            },
            {
                "extend": 'excelHtml5',
                "text": '<span class="fa fa-file-excel-o"></span>',
                "orientation": "landscape",
                "title": 'Historial de Puntos: '+$('#panelista-name').html(),
                "titleAttr": 'EXCEL'
            },
            {
                "extend": 'pdfHtml5',
                "text": '<span class="fa fa-file-pdf-o"></span>',
                "orientation": "landscape",
                "title": 'Historial de Puntos: '+$('#panelista-name').html(),
                "titleAttr": 'PDF'
            }
        ],
        "columns": [{ "width": "40%" },{ "width": "20%" },{ "width": "20%" },{ "width": "20%" }]
};