$(function () {

    'use strict';

    $.ajax({
        data: '',
        url:   'http://'+document.domain+'/dashboard/report_users',
        type:  'GET',
        beforeSend: function () {
            $('#reporteUsuariosTitle').html("Cargando...");
        },
        success:  function (response) {
            if(response){
                $('#reporteUsuariosTitle').html('Intérvalo: '+response.fechas[0]+' - '+response.fechas[[response.fechas.length-1]]);

                var registrados = 0;
                var invitados = 0;
                for (var i=0; i < response.registrados.length; i++) {
                    registrados += response.registrados[i];
                    invitados += response.invitados[i];
                }

                $('#reporteUsuariosChartTotalRegistrados').html('Usuarios Registrados: '+registrados);
                $('#reporteUsuariosChartTotalInvitados').html('Usuarios Invitados: '+invitados);

                //-----------------------
                //- MONTHLY SALES CHART -
                //-----------------------

                // Get context with jQuery - using jQuery's .get() method.
                var salesChartCanvas = $("#reporteUsuariosChart").get(0).getContext("2d");
                // This will get the first returned node in the jQuery collection.
                var salesChart = new Chart(salesChartCanvas);

                var salesChartData = {
                  labels: response.fechas,
                  datasets: [
                    {
                      label: "Usuarios Invitados",
                      fillColor: "rgb(210, 214, 222)",
                      strokeColor: "rgb(210, 214, 222)",
                      pointColor: "rgb(210, 214, 222)",
                      pointStrokeColor: "#c1c7d1",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgb(220,220,220)",
                      data: response.invitados
                    },
                    {
                      label: "Usuarios Registrados",
                      fillColor: "rgba(60,141,188,0.9)",
                      strokeColor: "rgba(60,141,188,0.8)",
                      pointColor: "#3b8bba",
                      pointStrokeColor: "rgba(60,141,188,1)",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(60,141,188,1)",
                      data: response.registrados
                    }
                  ]
                };

                var salesChartOptions = {
                  //Boolean - If we should show the scale at all
                  showScale: true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines: false,
                  //String - Colour of the grid lines
                  scaleGridLineColor: "rgba(0,0,0,.05)",
                  //Number - Width of the grid lines
                  scaleGridLineWidth: 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines: true,
                  //Boolean - Whether the line is curved between points
                  bezierCurve: true,
                  //Number - Tension of the bezier curve between points
                  bezierCurveTension: 0.3,
                  //Boolean - Whether to show a dot for each point
                  pointDot: false,
                  //Number - Radius of each point dot in pixels
                  pointDotRadius: 4,
                  //Number - Pixel width of point dot stroke
                  pointDotStrokeWidth: 1,
                  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                  pointHitDetectionRadius: 20,
                  //Boolean - Whether to show a stroke for datasets
                  datasetStroke: true,
                  //Number - Pixel width of dataset stroke
                  datasetStrokeWidth: 2,
                  //Boolean - Whether to fill the dataset with a color
                  datasetFill: true,
                  //String - A legend template
                  legendTemplate: "<ul class=\"chart-legend clearfix\"><li><span class=\"fa fa-circle-o text-red\"></span>Label</li></ul>",
                  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio: true,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive: true,

                };

                //Create the line chart
                salesChart.Line(salesChartData, salesChartOptions);

                //---------------------------
                //- END MONTHLY SALES CHART -
                //---------------------------

            }
        }
    });

    $.ajax({
        data: '',
        url:   'http://'+document.domain+'/dashboard/app_use',
        type:  'GET',
        beforeSend: function () {
            $('#appUseChartTitle').html("Cargando...");
        },
        success:  function (response) {
            if(response){
                $('#appUseChartTitle').html("");
                $('#appUseChartUl').html("<li><i class=\"fa fa-circle-o text-red\"></i> "+response.applications[0]['app_name']+"</li><li><i class=\"fa fa-circle-o text-green\"></i> "+response.applications[1]['app_name']+"</li><li><i class=\"fa fa-circle-o text-yellow\"></i> "+response.applications[2]['app_name']+"</li><li><i class=\"fa fa-circle-o text-aqua\"></i> "+response.applications[3]['app_name']+"</li><li><i class=\"fa fa-circle-o text-light-blue\"></i> "+response.applications[4]['app_name']+"</li><li><i class=\"fa fa-circle-o text-gray\"></i> "+response.applications[5]['app_name']+"</li>");

                /* ChartJS
                 * -------
                 * Here we will create a few charts using ChartJS
                 */

                //-------------
                //- PIE CHART -
                //-------------
                // Get context with jQuery - using jQuery's .get() method.
                var pieChartCanvas = $("#appUseChart").get(0).getContext("2d");
                var pieChart = new Chart(pieChartCanvas);
                var PieData = [
                  {
                    value: (response.applications[0]['app_duracion']/3600).toFixed(2),
                    color: "#f56954",
                    highlight: "#f56954",
                    label: response.applications[0]['app_name']
                  },
                  {
                    value: (response.applications[1]['app_duracion']/3600).toFixed(2),
                    color: "#00a65a",
                    highlight: "#00a65a",
                    label: response.applications[1]['app_name']
                  },
                  {
                    value: (response.applications[2]['app_duracion']/3600).toFixed(2),
                    color: "#f39c12",
                    highlight: "#f39c12",
                    label: response.applications[2]['app_name']
                  },
                  {
                    value: (response.applications[3]['app_duracion']/3600).toFixed(2),
                    color: "#00c0ef",
                    highlight: "#00c0ef",
                    label: response.applications[3]['app_name']
                  },
                  {
                    value: (response.applications[4]['app_duracion']/3600).toFixed(2),
                    color: "#3c8dbc",
                    highlight: "#3c8dbc",
                    label: response.applications[4]['app_name']
                  },
                  {
                    value: (response.applications[5]['app_duracion']/3600).toFixed(2),
                    color: "#d2d6de",
                    highlight: "#d2d6de",
                    label: response.applications[5]['app_name']
                  }
                ];
                var pieOptions = {
                  //Boolean - Whether we should show a stroke on each segment
                  segmentShowStroke: true,
                  //String - The colour of each segment stroke
                  segmentStrokeColor: "#fff",
                  //Number - The width of each segment stroke
                  segmentStrokeWidth: 1,
                  //Number - The percentage of the chart that we cut out of the middle
                  percentageInnerCutout: 50, // This is 0 for Pie charts
                  //Number - Amount of animation steps
                  animationSteps: 100,
                  //String - Animation easing effect
                  animationEasing: "easeOutBounce",
                  //Boolean - Whether we animate the rotation of the Doughnut
                  animateRotate: true,
                  //Boolean - Whether we animate scaling the Doughnut from the centre
                  animateScale: false,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive: true,
                  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio: false,
                  //String - A legend template
                  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                  //String - A tooltip template
                  tooltipTemplate: "<%=value%>hr de uso"
                };
                //Create pie or douhnut chart
                // You can switch between pie and douhnut using the method below.
                pieChart.Doughnut(PieData, pieOptions);
                //-----------------
                //- END PIE CHART -
                //-----------------

            }
        }
    });

  $("#modal-dashboard").on('click', function (e) {
      e.preventDefault();
      var title = document.getElementById('modal-dashboard-title');
      $(title).html('');
  });

});
