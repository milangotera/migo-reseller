function mostrarFecha(days, option){
    milisegundos=parseInt(35*24*60*60*1000);
    fecha=new Date();
    day=fecha.getDate();
    if(day < 10){ day = '0'+day; }
    month=fecha.getMonth()+1;
    if(month < 10){ month = '0'+month; }
    year=fecha.getFullYear();
    var fecha_actual = year+"-"+month+"-"+day;
    tiempo=fecha.getTime();
    milisegundos=parseInt(days*24*60*60*1000);
    total=fecha.setTime(tiempo+milisegundos);
    day=fecha.getDate();
    if(day < 10){ day = '0'+day; }
    month=fecha.getMonth()+1;
    if(month < 10){ month = '0'+month; }
    year=fecha.getFullYear();

    if(option == 'rango'){
        return year+"-"+month+"-"+day+" - "+fecha_actual;
    }
    if(option == 'dia'){
        return year+"-"+month+"-"+day+" - "+year+"-"+month+"-"+day;
    }
}

function contarMensaje(campo, limite, span){
    var campo = document.getElementById(campo);
    if(campo.value.length == 0){
        $('#'+span).html('Debe ingresar el mensaje de la notificación');
        $('#bottom-cotinuar-mensaje').attr('disabled','disabled');
        return 0;
    }
    else if(campo.value.length > limite){
        $('#'+span).html('El mensaje no puede tener más de 110 caracteres');
        $('#bottom-cotinuar-mensaje').attr('disabled','disabled');
        return 0;
    }
    else{
        $('#'+span).html(campo.value.length+'/'+limite);
        $("#bottom-cotinuar-mensaje").removeAttr('disabled');
        return 1;
    }
}

function continuar(div){
    if( div == 1 ){
        $('#div-filtrar').addClass("in");
        $('#div-mensaje').removeClass("in");
        $('#div-enviar').removeClass("in");

        $('#icono-div-filtrar').addClass("ocultar");
        $('#icono-div-mensaje').addClass("ocultar");
    }
    if( div == 2 ){
        $('#div-mensaje').addClass("in");
        $('#div-filtrar').removeClass("in");
        $('#div-enviar').removeClass("in");

        $('#icono-div-filtrar').removeClass("ocultar");
        $('#icono-div-mensaje').addClass("ocultar");
    }
    if( div == 3 ){
        $('#div-enviar').addClass("in");
        $('#div-filtrar').removeClass("in");
        $('#div-mensaje').removeClass("in");

        $('#icono-div-mensaje').removeClass("ocultar");
    }
}

function enviarMensajes (inicio, fin){
    var panelistas = document.getElementById('panelistas').value.split(",");
    var path = document.getElementById('path-3');
    var mensaje = document.getElementById('mensaje-2');
    var enviadas = document.getElementById('enviadas');
    var fallidas = document.getElementById('fallidas');
    var url = '';
    if($("#url").prop('disabled')!=true){
      url = $("#url").val();
    }
    $.ajax({
               async: true,
               data: { "mensaje": mensaje.value, "tipo": "push", "panelista": panelistas[inicio], "url": url },
               url:  path.value,
               type:  'POST',
               beforeSend: function () {
                    $('#enviar-final').attr('disabled','disabled');
                    $('#resultado-final').html(
                        '<b>Enviando... <span class=\'enviados\'>'+inicio+'</span> de '+fin+
                        '</b><br><br>'
                    );
               },
               success:  function (response) {
                   if(response.result == 1){
                       enviadas.value = parseInt(enviadas.value) + 1;
                   }
                   if(response.result == 0){
                       fallidas.value = parseInt(fallidas.value) + 1;
                   }
                   inicio++;
                   if(inicio < fin){
                       enviarMensajes (inicio, fin);
                   }
                   if(inicio == fin){
                       var fuente = $('#contador-push-masivo').html();  
                       var plantilla = Handlebars.compile(fuente);
                       var datos = {  
                         enviadas: enviadas.value,
                         fallidas: fallidas.value
                       };
                       var resultadoFinal = plantilla(datos);
                       $('#resultado-final').html(resultadoFinal);
                   }
               }
    });
}

$(function(){
    var $fechaSync = $('#fecha_sincronizacion'),
        fechas = [
            {value:'any', text:'Cualquiera'},
            {value:mostrarFecha(0,'dia'), text:'Hoy'},
            {value:mostrarFecha(-1,'dia'), text:'Ayer'},
            {value:mostrarFecha(-2,'rango'), text:'Últimos 3 días'},
            {value:mostrarFecha(-6,'rango'), text:'Últimos 7 días'},
            {value:mostrarFecha(-29,'rango'), text:'Últimos 30 días'},
            {value:'rango_fechas', text:'Por rango de fechas'}
        ]
    ;

    $.each(fechas, function(inx, item){
        $fechaSync.append($('<option/>', {
            text: item.text,
            val: item.value
        }));
    });

    $('select').select2();

    $fechaSync
        .on('change', function(event){
            event.preventDefault();
            $("#fecha_rango").prop(
                'disabled',
                $(this).find('option:selected').val() != 'rango_fechas'
            );
        })
        .change()
    ;

    $('#fecha_rango').daterangepicker({
        locale: {
         format: 'YYYY-MM-DD'
       }
    });

    $('body')
      .on('change', '#aplicacion', function (event) {
        event.preventDefault();
        $(".grupo-familiar").prop('disabled',$(this).find('option:selected').val() != 2);
      })
      .change()
      .on('change', '#fechaSync', function (event) {
        event.preventDefault();
        $("#fecha_rango").prop('disabled',$(this).find('option:selected').val() != 'rango_fechas');
      })
      .change()
      .on('click', '#button-filtrar', function (event) {
        event.preventDefault();
        var aplicacion = document.getElementById('aplicacion');
        var sexo = document.getElementById('sexo');
        var grupo_familiar = document.getElementById('grupo_familiar');
        var codigo_postal = document.getElementById('codigo_postal');
        var panelistas = document.getElementById('panelistas'); panelistas.value = '';
        var path = document.getElementById('path');
        var fecha_rango = document.getElementById('fecha_rango');
        var fecha_sincronizacion = document.getElementById('fecha_sincronizacion');
        var validar = 0;
        if(codigo_postal.value != ''){
            validar = inputPostalCode('codigo_postal');
        }
        if(validar == 0){
            $.ajax({
               data: $('#form_filtrar_send_push').serialize(),
               url:   $("#path").val(),
               type:  'POST',
               beforeSend: function () {
                   $('#form_filtrar_send_push').find('input, textarea, button, select, radio').attr('disabled','disabled');
                   $('#div-respuesta-filtrar').removeClass("ocultar");
                   $('#div-button-continuar').addClass("ocultar");
                   $('#respuesta-filtrar').html("Espere...");
               },
               success:  function (response) {
                   $('#form_filtrar_send_push').find('input, textarea, button, select, radio').removeAttr("disabled");
                   if(aplicacion.value != '2'){
                       $('#grupo_familiar').attr('disabled','disabled');
                   }
                   if(fecha_sincronizacion.value != 'rango_fechas'){
                       $('#fecha_rango').attr('disabled','disabled');
                   }
                   $('#respuesta-filtrar').html(response.message);
                   if(response.result == 1){
                       $('#div-button-continuar').removeClass("ocultar");
                       var todos = [];
                       for (var i=0; i < response.panelistas.length; i++) {
                          todos.push(response.panelistas[i]['id_panelista']);
                       }
                       panelistas.value = [todos];
                       $('#texto-mensaje').html('0/110');
                       $('#bottom-cotinuar-mensaje').attr('disabled','disabled');
                   }
                   if(response.result == 0){

                   }
               }
            });
        }
    })
    .on('click', '#bottom-cotinuar-mensaje', function (event) {
        event.preventDefault();
        var panelistas = document.getElementById('panelistas');
        var path = document.getElementById('path-2');
        var mensaje = document.getElementById('mensaje');
        var mensaje2 = document.getElementById('mensaje-2');

        var validarMensaje = contarMensaje('mensaje', 110, 'texto-mensaje');
        var validarUrl = true;
        if($("#url").prop('disabled')!=true){
            validarUrl = is_url($('#url').val());
            if(validarUrl != true){
              $('#texto-url').html('Debe ingresar un enlace para abrir en la Web');
              $("#url").focus();
            }
            else{
              $('#texto-url').html('');
            }
        }

        if(validarMensaje == 1 && validarUrl == true){
            mensaje2.value = mensaje.value;
            $('#total-enviar').html($('#total').html());
            $('#total-usuarios').html($('#total').html());
            continuar(3);
        }

    })
    .on('click', '#enviar-final', function (event) {
        event.preventDefault();
        var inicio = 0;
        enviarMensajes (inicio, document.getElementById('panelistas').value.split(",").length);
    })
    .on('click', '#enviar-a-modal', function (event) {
        event.preventDefault();
        $('#resultado-final').html('');
        $('#enviar-final').removeAttr("disabled");
        $('#enviadas').val("0");
        $('#fallidas').val("0");
    })
    .on('click', '#url-checkbox', function (event) {
        if($(this).prop("checked") == true){
          $("#url").prop('disabled', false);
        }
        else{
          $("#url").prop('disabled', true);
        }
        $("#url").val('https://');
    })
    .on('keyup', '#url', function (event) {
        var formato = "https://";
        if(
            ($(this).val() == formato.substring(0, 7)) ||
            ($(this).val() == formato.substring(0, 6)) ||
            ($(this).val() == formato.substring(0, 5))
          )
        {
          $(this).val(formato);
        }
        if($(this).val() != formato && formato.indexOf($(this).val())!=-1){
          $(this).val(formato+$(this).val());
        }
    });

});
