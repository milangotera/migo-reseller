<?php

namespace Migo\Reseller\AdminBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Sex;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Sex controller.
 *
 * @Route("sex")
 */
class SexController extends Controller
{
    /**
     * Lists all sex entities.
     *
     * @Route("/", name="sex_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sexes = $em->getRepository('MigoResellerBaseBundle:Sex')->findAll();

        return $this->render('sex/index.html.twig', array(
            'sexes' => $sexes,
        ));
    }

    /**
     * Creates a new sex entity.
     *
     * @Route("/new", name="sex_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sex = new Sex();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\SexType', $sex);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sex);
            $em->flush();

            return $this->redirectToRoute('sex_show', array('id' => $sex->getId()));
        }

        return $this->render('sex/new.html.twig', array(
            'sex' => $sex,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sex entity.
     *
     * @Route("/{id}", name="sex_show")
     * @Method("GET")
     */
    public function showAction(Sex $sex)
    {
        $deleteForm = $this->createDeleteForm($sex);

        return $this->render('sex/show.html.twig', array(
            'sex' => $sex,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sex entity.
     *
     * @Route("/{id}/edit", name="sex_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Sex $sex)
    {
        $deleteForm = $this->createDeleteForm($sex);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\SexType', $sex);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sex_edit', array('id' => $sex->getId()));
        }

        return $this->render('sex/edit.html.twig', array(
            'sex' => $sex,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sex entity.
     *
     * @Route("/{id}", name="sex_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sex $sex)
    {
        $form = $this->createDeleteForm($sex);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sex);
            $em->flush();
        }

        return $this->redirectToRoute('sex_index');
    }

    /**
     * Creates a form to delete a sex entity.
     *
     * @param Sex $sex The sex entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sex $sex)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sex_delete', array('id' => $sex->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
