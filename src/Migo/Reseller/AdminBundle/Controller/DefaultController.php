<?php

namespace Migo\Reseller\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Default controller.
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     * @Method("GET")
     * @Template()
     */
    public function baseUrlAction()
    {
        return $this->redirectToRoute('home_page');
    }
}