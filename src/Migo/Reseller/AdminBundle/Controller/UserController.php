<?php

namespace Migo\Reseller\AdminBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('MigoResellerBaseBundle:User')->findAll();

        return [
            'users' => $users,
        ];
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $errors      = '';
        $nombre      = '';
        $apellido    = '';
        $usuario     = '';
        $email       = '';
        $password    = '';
        $estatus     = 1;
        $role_id     = 1;

        if($request->getMethod() == 'POST'){
            $fecha =  date("Y-m-d");
            $hora  =  date("H:i:s");

            $nombre      = $request->get("nombre") ? $request->request->get("nombre") : '';
            $apellido    = $request->get("apellido") ? $request->request->get("apellido") : '';
            $usuario     = $request->get("usuario") ? $request->request->get("usuario") : '';
            $email       = $request->get("email") ? $request->request->get("email") : '';
            $password    = $request->get("password") ? $request->request->get("password") : '';
            $estatus     = $request->get("estatus") ? $request->request->get("estatus") : 0;
            $role_id     = $request->get("role_id") ? $request->request->get("role_id") : 1;

            $em = $this->getDoctrine()->getManager();
            $role = $em->getRepository('MigoResellerBaseBundle:Role')->find($role_id);
            $cuenta = new User();
            $cuenta->setRoles($role);
            $cuenta->setFirstName($nombre);
            $cuenta->setLastName($apellido);
            $cuenta->setUserName($usuario);
            $cuenta->setEmail($email);
            $cuenta->setMonto(0);
            $cuenta->setIsActive(intval($estatus));
            $cuenta->setCreatedAt($fecha.' '.$hora);
            $cuenta->setUpdatedAt($fecha.' '.$hora);
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($cuenta);
            $password = $encoder->encodePassword($password, $cuenta->getSalt());
            $cuenta->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $validator = $this->get('validator');
            $errors = $validator->validate($cuenta);
            if (count($errors) == 0) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($cuenta);
                $em->flush();
                $this->addFlash('success', 'Cuenta registrada exitosamente.');
                return $this->redirectToRoute('user_index');
            }
        }

        $data = [
            'errors' => $errors,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'usuario' => $usuario,
            'email' => $email,
            'password' => $password,
            'estatus' => $estatus,
            'role_id' => $role_id,
        ];

        return [
            'data' => $data
        ];
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return [
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return [
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}/delete", name="user_delete")
     * @Method("GET")
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MigoResellerBaseBundle:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
