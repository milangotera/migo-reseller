<?php

namespace Migo\Reseller\AdminBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Rental;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Rental controller.
 *
 * @Route("rental")
 */
class RentalController extends Controller
{
    /**
     * Lists all rental entities.
     *
     * @Route("/", name="rental_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rentals = $em->getRepository('MigoResellerBaseBundle:Rental')->findAll();

        return [
            'rentals' => $rentals,
        ];
    }

    /**
     * Creates a new rental entity.
     *
     * @Route("/new", name="rental_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $rental = new Rental();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\RentalModeradorType', $rental);
        $form->handleRequest($request);
        $saldo = 0;
        $meses = 1;
        $status  = $em->getRepository('MigoResellerBaseBundle:Status')->find(1);
        $type     = $em->getRepository('MigoResellerBaseBundle:RentalType')->find(1);
        $accounts = $em->getRepository('MigoResellerBaseBundle:Account')->findAvailable();
        if ($form->isSubmitted() && $form->isValid()) {
            $moderador = $em->getRepository('MigoResellerBaseBundle:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
            $reseller = $em->getRepository('MigoResellerBaseBundle:User')->find($rental->getReseller()->getId());
            $account = $em->getRepository('MigoResellerBaseBundle:Account')->find($request->request->get("account"));
            $meses = intval($rental->getMonths());
            $today = date("Y-m-d");
            $start = date("Y-m-d");
            $end   = date("Y-m-d", strtotime("$today + $meses months"));
            if($meses > 0){
                $saldo = ($reseller->getMonto() - ($account->getMonto() * $meses));
                if($saldo >= 0 && intval($rental->getMonths()) > 0){
                    $rental->setUser($moderador);
                    $reseller->setMonto($saldo);
                    $rental->setAccount($account);
                    $rental->setReseller($reseller);
                    $rental->setType($type);
                    $rental->setDate(new \DateTime($today));
                    $rental->setDateStart(new \DateTime($start));
                    $rental->setDateEnd(new \DateTime($end));
                    $rental->setStatus($status);
                    $em->persist($rental);
                    $em->flush();
                    return $this->redirectToRoute('rental_index');
                }
            }
        }

        return [
            'accounts' => $accounts,
            'saldo' => $saldo,
            'meses' => $meses,
            'rental' => $rental,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a rental entity.
     *
     * @Route("/{id}", name="rental_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Rental $rental)
    {
        $deleteForm = $this->createDeleteForm($rental);

        return [
            'rental' => $rental,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing rental entity.
     *
     * @Route("/{id}/edit", name="rental_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Rental $rental)
    {
        $deleteForm = $this->createDeleteForm($rental);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\RentalModeradorEditType', $rental);
        $editForm->handleRequest($request);
        $saldo = 0;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('rental_index');
        }

        return [
            'rental' => $rental,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a rental entity.
     *
     * @Route("/{id}", name="rental_delete")
     * @Method("DELETE")
     * @Template()
     */
    public function deleteAction(Request $request, Rental $rental)
    {
        $form = $this->createDeleteForm($rental);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rental);
            $em->flush();
        }

        return $this->redirectToRoute('rental_index');
    }

    /**
     * Creates a form to delete a rental entity.
     *
     * @param Rental $rental The rental entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Rental $rental)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rental_delete', array('id' => $rental->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
