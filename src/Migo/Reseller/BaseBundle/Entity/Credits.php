<?php

namespace Migo\Reseller\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Credits
 *
 * @ORM\Table(name="credits")
 * @ORM\Entity(repositoryClass="Migo\Reseller\BaseBundle\Repository\CreditsRepository")
 */
class Credits
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="credit", type="float")
     */
    private $credit;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=255)
     */
    private $method;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\User", inversedBy="credits")
     * @ORM\JoinColumn(name="admin_id", referencedColumnName="id")
     */
    private $admin;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\User", inversedBy="credits")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="string", length=255)
     */
    private $operation;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\CreditsStatus", inversedBy="rental")
     * @ORM\JoinColumn(name="status", referencedColumnName="id")
     */
    private $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set credit
     *
     * @param float $credit
     *
     * @return Credits
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return float
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set method
     *
     * @param string $method
     *
     * @return Credits
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Credits
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set admin
     *
     * @param \Migo\Reseller\BaseBundle\Entity\User $admin
     *
     * @return Credits
     */
    public function setAdmin(\Migo\Reseller\BaseBundle\Entity\User $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return \Migo\Reseller\BaseBundle\Entity\User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set user
     *
     * @param \Migo\Reseller\BaseBundle\Entity\User $user
     *
     * @return Credits
     */
    public function setUser(\Migo\Reseller\BaseBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Migo\Reseller\BaseBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set operation
     *
     * @param string $operation
     *
     * @return Credits
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation
     *
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set status
     *
     * @param \Migo\Reseller\BaseBundle\Entity\CreditsStatus $status
     *
     * @return Credits
     */
    public function setStatus(\Migo\Reseller\BaseBundle\Entity\CreditsStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Migo\Reseller\BaseBundle\Entity\CreditsStatus
     */
    public function getStatus()
    {
        return $this->status;
    }
}
