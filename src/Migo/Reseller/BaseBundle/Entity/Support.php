<?php

namespace Migo\Reseller\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Support
 *
 * @ORM\Table(name="support")
 * @ORM\Entity(repositoryClass="Migo\Reseller\BaseBundle\Repository\SupportRepository")
 */
class Support
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\User", inversedBy="rental")
     * @ORM\JoinColumn(name="reseller_id", referencedColumnName="id")
     */
    private $reseller;

    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="string", length=255)
     */
    private $operation;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="response", type="string", length=255)
     */
    private $response;

        /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\CreditsStatus", inversedBy="rental")
     * @ORM\JoinColumn(name="status", referencedColumnName="id")
     */
    private $status;

    public function __toString()
    {
      return $this->getreseller()->getUserName()." ".$this->getOperation();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Support
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set operation
     *
     * @param string $operation
     *
     * @return Support
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation
     *
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Support
     */
    public function setDate($date)
    {
        $this->date = new \DateTime($date);

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set reseller
     *
     * @param \Migo\Reseller\BaseBundle\Entity\User $reseller
     *
     * @return Support
     */
    public function setReseller(\Migo\Reseller\BaseBundle\Entity\User $reseller = null)
    {
        $this->reseller = $reseller;

        return $this;
    }

    /**
     * Get reseller
     *
     * @return \Migo\Reseller\BaseBundle\Entity\User
     */
    public function getReseller()
    {
        return $this->reseller;
    }

    /**
     * Set status
     *
     * @param \Migo\Reseller\BaseBundle\Entity\CreditsStatus $status
     *
     * @return Support
     */
    public function setStatus(\Migo\Reseller\BaseBundle\Entity\CreditsStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Migo\Reseller\BaseBundle\Entity\CreditsStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set response
     *
     * @param string $response
     *
     * @return Support
     */
    public function setResponse($response)
    {
        if($response)
            $this->response = $response;
        else
            $this->response = "En espera...";

        return $this;
    }

    /**
     * Get response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }
}
