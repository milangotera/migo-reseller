<?php

namespace Migo\Reseller\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sale
 *
 * @ORM\Table(name="sale")
 * @ORM\Entity(repositoryClass="Migo\Reseller\BaseBundle\Repository\SaleRepository")
 */
class Sale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\Account", inversedBy="sale")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="profiles", type="integer")
     */
    private $profiles;

    /**
     * @var string
     *
     * @ORM\Column(name="payment", type="integer")
     */
    private $payment;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\Client", inversedBy="sale")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Migo\Reseller\BaseBundle\Entity\User", inversedBy="rental")
     * @ORM\JoinColumn(name="reseller_id", referencedColumnName="id")
     */
    private $reseller;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=255)
     */
    private $method;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="datetime")
     */
    private $dateEnd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="months", type="integer")
     */
    private $months;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profiles
     *
     * @param integer $profiles
     *
     * @return Sale
     */
    public function setProfiles($profiles)
    {
        $this->profiles = $profiles;

        return $this;
    }

    /**
     * Get profiles
     *
     * @return integer
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * Set payment
     *
     * @param integer $payment
     *
     * @return Sale
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return integer
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set method
     *
     * @param string $method
     *
     * @return Sale
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Sale
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Sale
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Sale
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set months
     *
     * @param integer $months
     *
     * @return Sale
     */
    public function setMonths($months)
    {
        $this->months = $months;

        return $this;
    }

    /**
     * Get months
     *
     * @return integer
     */
    public function getMonths()
    {
        return $this->months;
    }

    /**
     * Set account
     *
     * @param \Migo\Reseller\BaseBundle\Entity\Account $account
     *
     * @return Sale
     */
    public function setAccount(\Migo\Reseller\BaseBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Migo\Reseller\BaseBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param \Migo\Reseller\BaseBundle\Entity\Client $user
     *
     * @return Sale
     */
    public function setUser(\Migo\Reseller\BaseBundle\Entity\Client $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Migo\Reseller\BaseBundle\Entity\Client
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set reseller
     *
     * @param \Migo\Reseller\BaseBundle\Entity\User $reseller
     *
     * @return Sale
     */
    public function setReseller(\Migo\Reseller\BaseBundle\Entity\User $reseller = null)
    {
        $this->reseller = $reseller;

        return $this;
    }

    /**
     * Get reseller
     *
     * @return \Migo\Reseller\BaseBundle\Entity\User
     */
    public function getReseller()
    {
        return $this->reseller;
    }
}
