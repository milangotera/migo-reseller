<?php

namespace Migo\Reseller\BaseBundle\Helper;

use Doctrine\DBAL\Driver\Connection;

class ViewHelper
{
    /**
    * @var \Doctrine\DBAL\Driver\Connection
    */
    public $connection;

    function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function execute($sql, $param = array())
    {
        $query = $this->connection->prepare($sql);
        $query->execute($param);
        return $query;
    }

    public function fetchAll($sql, $param = array())
    {
        return $this->execute($sql, $param)->fetchAll(\PDO::FETCH_ASSOC);
    }
}
