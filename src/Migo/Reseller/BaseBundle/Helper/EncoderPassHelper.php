<?php

namespace Migo\Lotery\BaseBundle\Helper;
 
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
 
class EncoderPassHelper implements PasswordEncoderInterface
{
 
    public function encodePassword($raw, $salt)
    {
        return hash('sha1', $salt . $raw); 
    }
 
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }
 
}
