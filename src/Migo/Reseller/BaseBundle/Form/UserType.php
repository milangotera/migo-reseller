<?php

namespace Migo\Reseller\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$role = $this->container->get('security.authorization_checker');
        $builder
            ->add('username', null, [
                'label' => 'Username', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('firstName', null, [
                'label' => 'Nombre', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('lastName', null, [
                'label' => 'Apellido', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('email', null, [
                'label' => 'Email', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('password', null, [
                'required'   => false,
                'label' => 'Password', 
                'attr' => ['class' => 'form-control', 'value' => ''],
            ])
            ->add('isActive', null, [
                'label' => 'Activo', 
                
            ])
            ->add('createdAt', DateType::class, [
                'label' => 'Registro',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('updatedAt', DateType::class, [
                'label' => 'Modificado',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('monto', HiddenType::class, [
                'label' => 'Créditos', 
                'attr' => ['class' => 'form-control'],
            ]);
            
            $builder->add('roles', null, [
                'label' => 'Roles', 
                'attr' => ['class' => 'form-control', 'required'=> 'required'],
            ]);
            
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Migo\Reseller\BaseBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'migo_reseller_basebundle_user';
    }


}
