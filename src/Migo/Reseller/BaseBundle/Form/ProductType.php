<?php

namespace Migo\Reseller\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', null, [
                'label' => 'Tipo', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('name', null, [
                'label' => 'Nombre', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('note', null, [
                'label' => 'Nota', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('price', null, [
                'label' => 'Costo', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('rental', null, [
                'label' => 'Cuentas', 
                'attr' => ['class' => 'form-control'],
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Migo\Reseller\BaseBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'migo_reseller_basebundle_product';
    }


}
