<?php

namespace Migo\Reseller\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AccountType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, [
                'label' => 'Email', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('password', null, [
                'label' => 'Password', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('createdAt', DateType::class, [
                'label' => 'Activación',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('updatedAt', DateType::class, [
                'label' => 'Vencimiento',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('status', null, [
                'label' => 'Estatus', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('country', null, [
                'label' => 'Pais', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('produc', null, [
                'label' => 'Producto', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('monto', null, [
                'label' => 'Créditos', 
                'attr' => ['class' => 'form-control'],
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Migo\Reseller\BaseBundle\Entity\Account'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'migo_reseller_basebundle_account';
    }


}
