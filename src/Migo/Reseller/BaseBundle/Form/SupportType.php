<?php

namespace Migo\Reseller\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class SupportType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('operation', null, [
                'label' => 'Operación', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('date', DateType::class, [
                'label' => 'Fecha', 
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('reseller', null, [
                'label' => 'Usuario', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('reseller', null, [
                'label' => 'Usuario', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('note', TextareaType::class, [
                'label' => 'Nota', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('response', TextareaType::class, [
                'label' => 'Respuesta', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('status', null, [
                'label' => 'Estatus', 
                'attr' => ['class' => 'form-control'],
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Migo\Reseller\BaseBundle\Entity\Support'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'migo_reseller_basebundle_support';
    }


}
