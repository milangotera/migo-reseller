<?php

namespace Migo\Reseller\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RentalModeradorEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', null, [
                'label' => 'Estatus',
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('dateStart', DateType::class, [
                'label' => 'Vendido el',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('dateEnd', DateType::class, [
                'label' => 'Vence el',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'form-control js-datepicker'],
            ])
            ->add('method', null, [
                'label' => 'Método',
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('months', null, [
                'label' => 'Meses',
                'required' => true,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('support', TextareaType::class, [
                'required' => true,
                'label' => 'Observación', 
                'attr' => ['class' => 'form-control'],
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Migo\Reseller\BaseBundle\Entity\Rental'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'migo_reseller_basebundle_rental';
    }


}
