<?php

namespace Migo\Reseller\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firsName', null, [
                'label' => 'Nombre', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('lastName', null, [
                'label' => 'Apellido', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('sex', null, [
                'label' => 'Sexo', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('phone', null, [
                'label' => 'Teléfono', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('email', null, [
                'label' => 'Email', 
                'attr' => ['class' => 'form-control'],
            ])
            ->add('country', null, [
                'label' => 'Pais', 
                'attr' => ['class' => 'form-control'],
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Migo\Reseller\BaseBundle\Entity\Client'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'migo_reseller_basebundle_client';
    }


}
