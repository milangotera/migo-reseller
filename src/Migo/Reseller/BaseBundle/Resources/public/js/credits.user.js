
var dataDatatableConfig = {
        "dom": 'lfBrtip',
        "buttons": [
            {
                "extend": 'excelHtml5',
                "text": '<span class="fa fa-file-excel-o"> EXCEL</span>',
                "title": 'Reporte de Ventas',
                "titleAttr": 'EXCEL'
            },
            {
                "extend": 'pdfHtml5',
                "text": '<span class="fa fa-file-excel-o"> PDF</span>',
                "title": 'Reporte de Ventas',
                "titleAttr": 'PDF'
            }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 1 ).footer() ).html(
                total.toFixed(2)
            );
        },
        "order": [[ 0, "desc" ]]
};