
var dataDatatableConfig = {
        "dom": 'lfBrtip',
        "buttons": [
            {
                "extend": 'excelHtml5',
                "text": '<span class="fa fa-file-excel-o"> EXCEL</span>',
                "title": 'Reporte de Clientes',
                "titleAttr": 'EXCEL'
            },
            {
                "extend": 'pdfHtml5',
                "text": '<span class="fa fa-file-excel-o"> PDF</span>',
                "title": 'Reporte de Clientes',
                "titleAttr": 'PDF'
            }
        ],
        "order": [[ 0, "desc" ]]
};