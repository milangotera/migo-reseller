<?php

namespace Migo\Reseller\BaseBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\RentalType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Rentaltype controller.
 *
 * @Route("rentaltype")
 */
class RentalTypeController extends Controller
{
    /**
     * Lists all rentalType entities.
     *
     * @Route("/", name="rentaltype_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rentalTypes = $em->getRepository('MigoResellerBaseBundle:RentalType')->findAll();

        return $this->render('rentaltype/index.html.twig', array(
            'rentalTypes' => $rentalTypes,
        ));
    }

    /**
     * Creates a new rentalType entity.
     *
     * @Route("/new", name="rentaltype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $rentalType = new Rentaltype();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\RentalTypeType', $rentalType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rentalType);
            $em->flush();

            return $this->redirectToRoute('rentaltype_show', array('id' => $rentalType->getId()));
        }

        return $this->render('rentaltype/new.html.twig', array(
            'rentalType' => $rentalType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a rentalType entity.
     *
     * @Route("/{id}", name="rentaltype_show")
     * @Method("GET")
     */
    public function showAction(RentalType $rentalType)
    {
        $deleteForm = $this->createDeleteForm($rentalType);

        return $this->render('rentaltype/show.html.twig', array(
            'rentalType' => $rentalType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing rentalType entity.
     *
     * @Route("/{id}/edit", name="rentaltype_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, RentalType $rentalType)
    {
        $deleteForm = $this->createDeleteForm($rentalType);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\RentalTypeType', $rentalType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rentaltype_edit', array('id' => $rentalType->getId()));
        }

        return $this->render('rentaltype/edit.html.twig', array(
            'rentalType' => $rentalType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a rentalType entity.
     *
     * @Route("/{id}", name="rentaltype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, RentalType $rentalType)
    {
        $form = $this->createDeleteForm($rentalType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rentalType);
            $em->flush();
        }

        return $this->redirectToRoute('rentaltype_index');
    }

    /**
     * Creates a form to delete a rentalType entity.
     *
     * @param RentalType $rentalType The rentalType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(RentalType $rentalType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rentaltype_delete', array('id' => $rentalType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
