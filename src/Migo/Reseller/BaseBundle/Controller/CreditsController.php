<?php

namespace Migo\Reseller\BaseBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Credits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Credit controller.
 *
 * @Route("credits")
 */
class CreditsController extends Controller
{
    /**
     * Lists all credit entities.
     *
     * @Route("/", name="credits_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $credits = $em->getRepository('MigoResellerBaseBundle:Credits')->findAll();

        return $this->render('credits/index.html.twig', array(
            'credits' => $credits,
        ));
    }

    /**
     * Creates a new credit entity.
     *
     * @Route("/new", name="credits_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $credit = new Credit();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\CreditsType', $credit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($credit);
            $em->flush();

            return $this->redirectToRoute('credits_show', array('id' => $credit->getId()));
        }

        return $this->render('credits/new.html.twig', array(
            'credit' => $credit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a credit entity.
     *
     * @Route("/{id}", name="credits_show")
     * @Method("GET")
     */
    public function showAction(Credits $credit)
    {
        $deleteForm = $this->createDeleteForm($credit);

        return $this->render('credits/show.html.twig', array(
            'credit' => $credit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing credit entity.
     *
     * @Route("/{id}/edit", name="credits_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Credits $credit)
    {
        $deleteForm = $this->createDeleteForm($credit);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\CreditsType', $credit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('credits_edit', array('id' => $credit->getId()));
        }

        return $this->render('credits/edit.html.twig', array(
            'credit' => $credit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a credit entity.
     *
     * @Route("/{id}", name="credits_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Credits $credit)
    {
        $form = $this->createDeleteForm($credit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($credit);
            $em->flush();
        }

        return $this->redirectToRoute('credits_index');
    }

    /**
     * Creates a form to delete a credit entity.
     *
     * @param Credits $credit The credit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Credits $credit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('credits_delete', array('id' => $credit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
