<?php

namespace Migo\Reseller\RevendedorBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Sale;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Sale controller.
 *
 * @Route("sales")
 */
class SaleController extends Controller
{
    /**
     * Lists all sale entities.
     *
     * @Route("/", name="revendedor_sale_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $mayoristaId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $sales = $em->getRepository('MigoResellerBaseBundle:Sale')->findByReseller($mayoristaId);

        return [
            'sales' => $sales,
        ];
    }

    /**
     * Creates a new sale entity.
     *
     * @Route("/new", name="revendedor_sale_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $error = 0;
        $max = 0;
        $sale = new Sale();
        $em = $this->getDoctrine()->getManager();
        $mayoristaId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $rentals = $em->getRepository('MigoResellerBaseBundle:Rental')->findBy(
            array(
                'reseller' => $mayoristaId,
                'status' => 1,
            )
        );
        $clients = $em->getRepository('MigoResellerBaseBundle:Client')->findBy(array('user' =>$mayoristaId));
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\SaleType', $sale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $account  = $request->get("account") ? $request->request->get("account") : 0;
            
            $reseller = $mayoristaId;
            $user     = $request->get("user") ? $request->request->get("user") : 0;
            $status   = 1;

            if($account && $reseller && $user){
                $start = date("Y-m-d");
                $end   = date("Y-m-d", strtotime("$start + 1 months"));
                $account = $em->getRepository('MigoResellerBaseBundle:Account')->find($account);
                $reseller = $em->getRepository('MigoResellerBaseBundle:User')->find($reseller);
                $user = $em->getRepository('MigoResellerBaseBundle:Client')->find($user);
                $sale->setStatus($status);
                $sale->setAccount($account);
                $sale->setReseller($reseller);
                $sale->setUser($user);
                $sale->setMonths(1);
                $sale->setDateStart(new \DateTime($start));
                $sale->setDateEnd(new \DateTime($end));
                $max = $account->getProduc()->getRental();
                $viewHelper = $this->get('migo.reseller.helper.view');
                $sales = $viewHelper->fetchAll("SELECT SUM(profiles) AS profiles FROM sale WHERE account_id=:account AND reseller_id=:reseller AND status=:status",
                array(
                    'account' => $account->getId(),
                    'reseller' => $mayoristaId,
                    'status'=> 1,
                ));

                if((intval($sales[0]['profiles'])+intval($sale->getProfiles())) <= intval($max)){
                    $em->persist($sale);
                    $em->flush();
                }
                else
                    $error = $max;
                
                return $this->redirectToRoute('revendedor_sale_index');
            }
        }

        return [
            'rentals' => $rentals,
            'clients' => $clients,
            'sale' => $sale,
            'form' => $form->createView(),
            'error' => $error,
        ];
    }

    /**
     * Finds and displays a sale entity.
     *
     * @Route("/{id}", name="revendedor_sale_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Sale $sale)
    {
        $deleteForm = $this->createDeleteForm($sale);

        return [
            'sale' => $sale,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing sale entity.
     *
     * @Route("/{id}/edit", name="revendedor_sale_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Sale $sale, $id)
    {
        $deleteForm = $this->createDeleteForm($sale);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\SaleType', $sale);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $mayoristaId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $rentals = $em->getRepository('MigoResellerBaseBundle:Rental')->findBy(
            array(
                'reseller' => $mayoristaId,
                'status' => 1,
            )
        );
        $clients = $em->getRepository('MigoResellerBaseBundle:Client')->findBy(array('user' =>$mayoristaId));
        $sales = $em->getRepository('MigoResellerBaseBundle:Sale')->find($id);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $account  = $request->get("account") ? $request->request->get("account") : 0;
            $user     = $request->get("user") ? $request->request->get("user") : 0;
            $status   = $request->get("status") ? $request->request->get("status") : 0;
            if($account && $reseller && $user){
                $account = $em->getRepository('MigoResellerBaseBundle:Account')->find($account);
                $user = $em->getRepository('MigoResellerBaseBundle:Client')->find($user);
                $sale->setStatus($status);
                $sale->setAccount($account);
                $sale->setUser($user);
                $this->getDoctrine()->getManager()->flush();
            }
            return $this->redirectToRoute('revendedor_sale_edit', array('id' => $sale->getId()));
        }

        return [
            'rentals' => $rentals,
            'clients' => $clients,
            'sales' => $sales,
            'sale' => $sale,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a sale entity.
     *
     * @Route("/{id}", name="revendedor_sale_delete")
     * @Method("DELETE")
     * @Template()
     */
    public function deleteAction(Request $request, Sale $sale)
    {
        $form = $this->createDeleteForm($sale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sale);
            $em->flush();
        }

        return $this->redirectToRoute('revendedor_sale_index');
    }

    /**
     * Creates a form to delete a sale entity.
     *
     * @param Sale $sale The sale entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sale $sale)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('revendedor_sale_delete', array('id' => $sale->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
