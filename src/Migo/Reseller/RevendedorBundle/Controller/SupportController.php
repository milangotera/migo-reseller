<?php

namespace Migo\Reseller\RevendedorBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Support;
use Migo\Reseller\BaseBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Support controller.
 *
 * @Route("support")
 */
class SupportController extends Controller
{
    /**
     * Lists all support entities.
     *
     * @Route("/", name="revendedor_support_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $mayoristaId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        
        $supports = $em->getRepository('MigoResellerBaseBundle:Support')->findByReseller($mayoristaId);

        return [
            'supports' => $supports,
        ];
    }

    /**
     * Creates a new support entity.
     *
     * @Route("/new", name="revendedor_support_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $errors      = '';
        $operation   = '';
        $note        = '';
        $date        = '';
        $reseller    = '';
        $status      = 1;

        if($request->getMethod() == 'POST'){
            
            $em = $this->getDoctrine()->getManager();
            $mayoristaId = $this->get('security.token_storage')->getToken()->getUser()->getId();
            $date  =  date("Y-m-d");
            $hora  =  date("H:i:s");

            $operation   = $request->get("operation") ? $request->request->get("operation") : '';
            $note        = $request->get("note") ? $request->request->get("note") : '';
            $reseller    = $em->getRepository('MigoResellerBaseBundle:User')->find($mayoristaId);
            $status      = $em->getRepository('MigoResellerBaseBundle:CreditsStatus')->find(1);

            $support = new Support();
            $support->setNote($note);
            $support->setOperation($operation);
            $support->setDate($date);
            $support->setReseller($reseller);
            $support->setResponse('Esperndo Respuesta...');
            $support->setStatus($status);

            $validator = $this->get('validator');
            $errors = $validator->validate($support);
            if (count($errors) == 0) {
                $em->persist($support);
                $em->flush();
                $this->addFlash('success', 'Soporte enviado de forma exitosa.');
                return $this->redirectToRoute('revendedor_support_index');
            }
        }

        $data = [
            'errors' => $errors,
            'operation' => $operation,
            'note' => $note,
        ];

        return [
            'data' => $data
        ];
    }

    /**
     * Finds and displays a support entity.
     *
     * @Route("/{id}", name="revendedor_support_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Support $support)
    {
        $deleteForm = $this->createDeleteForm($support);

        return [
            'support' => $support,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing support entity.
     *
     * @Route("/{id}/edit", name="revendedor_support_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Support $support)
    {
        $deleteForm = $this->createDeleteForm($support);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\SupportType', $support);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('revendedor_support_index');
        }

        return [
            'support' => $support,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a support entity.
     *
     * @Route("/{id}", name="revendedor_support_delete")
     * @Method("DELETE")
     * @Template()
     */
    public function deleteAction(Request $request, Support $support)
    {
        $form = $this->createDeleteForm($support);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($support);
            $em->flush();
        }

        return $this->redirectToRoute('revendedor_support_index');
    }

    /**
     * Creates a form to delete a support entity.
     *
     * @param Support $support The support entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Support $support)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('support_delete', array('id' => $support->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
