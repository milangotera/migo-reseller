<?php

namespace Migo\Reseller\ModeradorBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Credits;
use Migo\Reseller\BaseBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Credit controller.
 *
 * @Route("credits")
 */
class CreditsController extends Controller
{
    /**
     * Lists all credit entities.
     *
     * @Route("/", name="moderador_credits_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $mayoristaId = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $credits = $em->getRepository('MigoResellerBaseBundle:Credits')->findBy(array('user' =>$mayoristaId));

        return [
            'credits' => $credits,
        ];
    }

    /**
     *
     * @Route("/history/{id}/user", name="moderador_credits_history_user")
     * @Method("GET")
     * @Template()
     */
    public function userAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        //$credits = $em->getRepository('MigoResellerBaseBundle:Credits')->findOneBy(array('user'=>$id));

        $repository = $this->getDoctrine()
            ->getRepository('MigoResellerBaseBundle:Credits');
         
        $query = $repository->createQueryBuilder('p')
            ->where('p.user = :user')
            ->setParameter('user', $id)
            ->orderBy('p.id', 'DESC')
            ->getQuery();
         
        $credits = $query->getResult();

        return [
            'credits' => $credits,
        ];
    }

    /**
     * Creates a new credit entity.
     *
     * @Route("/new", name="moderador_credits_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $credit = new Credits();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\CreditsType', $credit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $userID = $em->getRepository('MigoResellerBaseBundle:User')->find($credit->getUser()->getId());
            $userID->setMonto($userID->getMonto() + $credit->getCredit());
            $adminID = $em->getRepository('MigoResellerBaseBundle:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
            $credit->setAdmin($adminID);
            $em->persist($credit);
            $em->flush();
            return $this->redirectToRoute('moderador_history_user', array('id' => $userID->getId()));
        }

        return [
            'credit' => $credit,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a credit entity.
     *
     * @Route("/{id}", name="moderador_credits_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Credits $credit)
    {
        $deleteForm = $this->createDeleteForm($credit);

        return [
            'credit' => $credit,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing credit entity.
     *
     * @Route("/{id}/edit", name="moderador_credits_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Credits $credit)
    {
        $deleteForm = $this->createDeleteForm($credit);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\CreditsType', $credit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('moderador_credits_edit', array('id' => $credit->getId()));
        }

        return [
            'credit' => $credit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a credit entity.
     *
     * @Route("/{id}", name="revendedor_credits_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Credits $credit)
    {
        $form = $this->createDeleteForm($credit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($credit);
            $em->flush();
        }

        return $this->redirectToRoute('moderador_credits_index');
    }

    /**
     * Creates a form to delete a credit entity.
     *
     * @param Credits $credit The credit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Credits $credit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('moderador_credits_delete', array('id' => $credit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
