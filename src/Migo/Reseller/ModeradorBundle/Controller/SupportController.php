<?php

namespace Migo\Reseller\ModeradorBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Support;
use Migo\Reseller\BaseBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Support controller.
 *
 * @Route("support")
 */
class SupportController extends Controller
{
    /**
     * Lists all support entities.
     *
     * @Route("/", name="moderador_support_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $supports = $em->getRepository('MigoResellerBaseBundle:Support')->findAll();

        return [
            'supports' => $supports,
        ];
    }

    /**
     * Creates a new support entity.
     *
     * @Route("/new", name="moderador_support_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $support = new Support();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\SupportType', $support);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($support);
            $em->flush();

            return $this->redirectToRoute('moderador_support_edit', array('id' => $support->getId()));
        }

        return [
            'support' => $support,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a support entity.
     *
     * @Route("/{id}", name="moderador_support_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Support $support)
    {
        $deleteForm = $this->createDeleteForm($support);

        return [
            'support' => $support,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing support entity.
     *
     * @Route("/{id}/edit", name="moderador_support_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Support $support)
    {
        $deleteForm = $this->createDeleteForm($support);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\SupportType', $support);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('moderador_support_index');
        }

        return [
            'support' => $support,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a support entity.
     *
     * @Route("/{id}", name="moderador_support_delete")
     * @Method("DELETE")
     * @Template()
     */
    public function deleteAction(Request $request, Support $support)
    {
        $form = $this->createDeleteForm($support);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($support);
            $em->flush();
        }

        return $this->redirectToRoute('moderador_support_index');
    }

    /**
     * Creates a form to delete a support entity.
     *
     * @param Support $support The support entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Support $support)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('moderador_support_delete', array('id' => $support->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
