<?php

namespace Migo\Reseller\ModeradorBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Account controller.
 *
 * @Route("account")
 */
class AccountController extends Controller
{
    /**
     * Lists all account entities.
     *
     * @Route("/", name="moderador_account_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $accounts = $em->getRepository('MigoResellerBaseBundle:Account')->findAll();

        return [
            'accounts' => $accounts,
        ];
    }

    /**
     * Creates a new account entity.
     *
     * @Route("/new", name="moderador_account_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $account = new Account();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\AccountType', $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('moderador_account_show', array('id' => $account->getId()));
        }

        return [
            'account' => $account,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a account entity.
     *
     * @Route("/{id}", name="moderador_account_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Account $account)
    {
        $deleteForm = $this->createDeleteForm($account);

        return [
            'account' => $account,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing account entity.
     *
     * @Route("/{id}/edit", name="moderador_account_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Account $account)
    {
        $deleteForm = $this->createDeleteForm($account);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\AccountType', $account);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('moderador_account_edit', array('id' => $account->getId()));
        }

        return [
            'account' => $account,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a account entity.
     *
     * @Route("/{id}/delete", name="moderador_account_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Account $account)
    {
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('MigoResellerBaseBundle:Account')->find($id);
        $em->remove($account);
        $em->flush();

        return $this->redirectToRoute('moderador_account_index');
    }

    /**
     * Creates a form to delete a account entity.
     *
     * @param Account $account The account entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Account $account)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('moderador_account_delete', array('id' => $account->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
