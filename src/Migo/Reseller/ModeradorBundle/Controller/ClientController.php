<?php

namespace Migo\Reseller\ModeradorBundle\Controller;

use Migo\Reseller\BaseBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Client controller.
 *
 * @Route("client")
 */
class ClientController extends Controller
{
    /**
     * Lists all client entities.
     *
     * @Route("/", name="moderador_client_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('MigoResellerBaseBundle:Client')->findAll();

        return [
            'clients' => $clients,
        ];
    }

    /**
     * Creates a new client entity.
     *
     * @Route("/new", name="moderador_client_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $client = new Client();
        $form = $this->createForm('Migo\Reseller\BaseBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('MigoResellerBaseBundle:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
            $client->setUser($user);
            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('moderador_client_edit', array('id' => $client->getId()));
        }

        return [
            'client' => $client,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a client entity.
     *
     * @Route("/{id}", name="moderador_client_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);

        return [
            'client' => $client,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing client entity.
     *
     * @Route("/{id}/edit", name="moderador_client_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction(Request $request, Client $client, $id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository('MigoResellerBaseBundle:Client')->find($id);
        if(!$clients)
            return $this->redirectToRoute('moderador_client_index');
        $deleteForm = $this->createDeleteForm($client);
        $editForm = $this->createForm('Migo\Reseller\BaseBundle\Form\ClientModeradorType', $client);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $user = $em->getRepository('MigoResellerBaseBundle:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
            $client->setUser($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('moderador_client_edit', array('id' => $client->getId()));
        }

        return [
            'client' => $client,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a client entity.
     *
     * @Route("/{id}/delete", name="moderador_client_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('MigoResellerBaseBundle:Client')->find($id);
        $em->remove($client);
        $em->flush();

        return $this->redirectToRoute('moderador_client_index');
    }

    /**
     * Creates a form to delete a client entity.
     *
     * @param Client $client The client entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Client $client)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('moderador_client_delete', array('id' => $client->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
