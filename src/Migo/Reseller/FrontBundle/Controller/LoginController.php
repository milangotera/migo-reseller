<?php

namespace Migo\Reseller\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class LoginController extends Controller
{

    /**
     * @Route("/login", name="login_login")
     * @Method("GET")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return [
        	'last_username' => $lastUsername,
        	'error' => $error
        ];
    }


    /**
     * @Route("/login_check", name="login_check")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function loginCheckAction(Request $request)
    {
        return $this->redirectToRoute('home_page');
    }

    /**
     * @Route("/logout", name="login_logout")
     */
    public function logoutAction(Request $request)
    {

    }

}