<?php

namespace Migo\AdminTemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class TemplateController extends Controller
{	
	/**
	 * @Route("/layout", name="template_layout")
     * @Method("GET")
     * @Template()
     */
    public function layoutAction()
    {
        return [];
    }

    /**
	 * @Route("/demo", name="template_demo")
     * @Method("GET")
     * @Template()
     */
    public function demoAction()
    {
        return [];
    }
}
