	var Notification = function(message) {};

    Notification.prototype.default = {
    		showHideTransition: 'plain',
			position: 'top-right',
    		heading: {
        		error: 'Error',
				warning: 'Warning',
				info: 'Informacion',
				success: 'Exito'
        },
        icon: {
        		error: 'error',
				warning: 'warning',
				info: 'info',
				success: 'success'
        }
    };

    Notification.prototype.error = function(message) {
    		this.render(this.default.heading.error, this.default.icon.error ,message);
    		return this;
    };

    Notification.prototype.warning = function(message) {
    		this.render(
        		this.default.heading.warning,
            this.default.icon.warning,
            message
        );
    		return this;
    };

    Notification.prototype.info = function(message) {
    		this.render(
        		this.default.heading.info,
            this.default.icon.info,
            message
        );
    		return this;
    };

    Notification.prototype.success = function(message) {
    		this.render(
        		this.default.heading.success,
            this.default.icon.success,
            message
        );
    		return this;
    };

    Notification.prototype.render = function(heading, icon, message) {
    		$.toast({
            heading: heading,
            text: message,
            showHideTransition: this.default.showHideTransition,
			position: this.default.position,
            icon: icon,
			hideAfter: 3000
        });
        return this;
    };

    var notificar = new Notification();
