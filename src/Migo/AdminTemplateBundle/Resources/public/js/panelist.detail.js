$(function() {
  
  $('#add-points').on('hidden.bs.modal', function (e) {
     limpiarForm('panelista_history_point_new');
  })

});

var dataDatatableConfig = {
        "paging": true,
        "lengthChange": false,
        "iDisplayLength": 3,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false,
        "language": {
             "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ Elementos",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando _START_ a _END_ de _TOTAL_",
             "sInfoEmpty":      "Mostrando 0 al 0 de 0",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar:",
             "sUrl":            "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": {
                 "sFirst":    "Primero",
                 "sLast":     "Último",
                 "sNext":     "»",
                 "sPrevious": "«"
             },
             "oAria": {
                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             }
         }
  }