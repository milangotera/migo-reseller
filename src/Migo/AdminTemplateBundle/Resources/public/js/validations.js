(function(a){a.fn.validCampoFranz=function(b){a(this).on({keypress:function(a){var c=a.which,d=a.keyCode,e=String.fromCharCode(c).toLowerCase(),f=b;(-1!=f.indexOf(e)||9==d||37!=c&&37==d||39==d&&39!=c||8==d||46==d&&46!=c)&&161!=c||a.preventDefault()}})}})(jQuery);

function is_url(str) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(str);
}

function convertDateFormat(string) {
  var info = string.split('-');
  return info[2].substring(0,2) + '/' + info[1] + '/' + info[0];
}

function convertDateNumber(string) {
  var info = string.split('-');
  return info[0] + info[1] + info[2];
}

function inputNumber(input)
{
    var selector = document.getElementById(input);
    if( isNaN(selector.value) || selector.value=='' ) {
        //notificar.error('Debe ingresar un valor numérico');
        selector.value='';
        selector.focus();
        return 0;
    }
    else{
        return 1;
    }
    selector.value = (selector.value + '').replace(/[^0-9]/g, '');
}

function inputPostalCode(input)
{
    var selector = document.getElementById(input);
    var span = input+"_mensaje";
    var validar = 0;
    var mensaje = '';
    var patron = /\¿+|\?+|\°+|\¬+|\|+|\!+|\#+|\$+|\%+|\&+|\\+|\=+|\'+|\¡+|\++|\*+|\~+|\[+|\]+|\{+|\}+|\^+|\<+|\>+|\"+|\@+|\(+|\)+|\:+|\;+|\.+|\,+|\/+|\`+/;
    if(selector.value != '') {
        if(selector.value.length < 4) {
            validar = 1;
            mensaje = 'No escribas menos de 4 caracteres.';
        }
        if(selector.value.length > 10) {
            validar = 2;
            mensaje = 'No escribas más de 10 caracteres.';
        }
        if(patron.test(selector.value)) {
            validar = 3;
            mensaje = 'El campo no permite caracteres especiales.';
        }
    }
    selector.value = selector.value.toUpperCase();
    $('#'+span).html(mensaje);
    return validar;
}

function simulateClick(control){
    $(control).click();
}

function familyTitle(title){
     $('#family_title').html(title);
}

function deshab(form, status) {
    if(status == false){
        $(form).find('input, textarea, button, select').removeAttr("disabled");
    }
    if(status == true){
        $(form).find('input, textarea, button, select').attr('disabled','disabled');
    }
}

function goToUrl(url)
{
    location.href = url;
}

function limpiarForm(name){
    document.getElementById(name).reset();
}

// Notificaciones por defecto
jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Rellena este campo.",
    email: "Escribe una dirección de correo válida",
    url: "Escribe una URL válida.",
    date: "Escribe una fecha válida.",
    dateISO: "Escribe una fecha (ISO) válida.",
    number: "Debe ingresar un valor numérico.",
    digits: "Escribe sólo dígitos.",
    creditcard: "Escribe un número de tarjeta válido.",
    equalTo: "Escribe el mismo valor de nuevo.",
    accept: "Escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Excedió el máximo de {0} caracteres."),
    minlength: jQuery.validator.format("No escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Escribe un valor mayor o igual a {0}.")
});

$().ready(function () {
    $("#formLogin").validate({
        debug: true,
        rules: {
            _username: { required: true, minlength:5 },
            _password: { required: true, minlength: 6 }
        },
        messages: {
            _username: {
                required: "Faltó por ingresar Usuario."
            },
            _password: {
                required: "Faltó por ingresar la Contraseña."
            }
        },
        errorElement: "span",
        submitHandler: function(form) {
          form.submit();
        }
    });
});

$(function(){
    $('#incentivos_puntos').validCampoFranz('0123456789');
});

function submitForm(name, method, url)
{
    var name = document.getElementById(name);
    var div = document.getElementById('respuesta');
    var total_puntos = document.getElementById('puntos_smartme');
    var puntos_nuevos = document.getElementById('incentivos_puntos');

    $("#panelista_history_point_new").validate({
        debug: true,
        rules: {
            "incentivos[puntos]": { required: true, max: 20000, min: 1, number: true },
            "incentivos[descripcion]": { required: true, maxlength: 110 }
        },
        messages: {
            "incentivos[puntos]": {
                required: "Debe ingresar un valor numérico.", max: "La cantidad Máxima permitida es 20.000", min: "La cantidad Mínima permitida es 1.", number: "EL formato permitido es solo números."
            },
            "incentivos[descripcion]": {
                required: "Favor ingrese un texto."
            }
        },
        errorElement: "span",
        submitHandler: function(form) {
            $.ajax({
                       data: $(name).serialize(),
                       url:   url,
                       type:  method,
                       beforeSend: function () {
                           deshab('#panelista_history_point_new', 'true');
                           $(div).html("<i class='fa fa-clock-o'></i>");
                       },
                       success:  function (response) {
                           deshab('#panelista_history_point_new', 'false');
                           if(response.result == 1){
                               $(div).html('');
                                if(total_puntos.innerHTML == 'N/A'){
                                    $(total_puntos).html(puntos_nuevos.value);
                                }
                                else {
                                     $(total_puntos).html(parseInt(puntos_nuevos.value) + parseInt(total_puntos.innerHTML));
                                }
                               document.getElementById("panelista_history_point_new").reset();
                               notificar.success(response.message);
                               simulateClick('#cerrar_modal');
                           }
                           if(response.result == 0){
                               notificar.error(response.message);
                           }
                       }
            });
        }
    });
}

(function($) {
    $(function() {
        var urlGravatar = $("#get_img").data('gravatar');
        $.ajax({
            url: urlGravatar,
            beforeSend: function(xhr) {
                xhr.overrideMimeType("text/plain; charset=x-user-defined");
            }
        }).done(function(){
        	$("#get_img").attr('src', $("#get_img").data('gravatar'));
            $("#get_img_2").attr('src', $("#get_img").data('gravatar'));
        });
    })
})(jQuery)
