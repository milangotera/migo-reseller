$(function() {

    $('#modal-panelis-send-push').on('hidden.bs.modal', function (e) {
         limpiarForm('form_panelist_send_push');
    });

    $('body')
        .on('click', '.push-masive-button', function (event) {
            var selected = '';    
            $('#panelist_list_form input[type=checkbox]').each(function(){
                if (this.checked) {
                    selected += $(this).val()+',';
                }
            }); 

            if (selected != ''){
                event.preventDefault();
                var fuente = $('#form-push-masivo').html();  
                var plantilla = Handlebars.compile(fuente);
                var datos = {  
                    valor: "push-masivo"
                };
                var resultadoFinal = plantilla(datos);
                $('#form_panelist_send_push').html(resultadoFinal);
                $('#panelistas').val(selected);
                var totales = document.getElementById('panelistas').value.split(",").length-1;
                $('#titulo-panelis-send-push').html("<div class=\"col-md-12\">Esta mensaje será enviado a "+totales+" destinatarios<br><br></div>");
                $('#name-panelis-send-push').html(" a panelitas");
                $('#modal-panelis-send-push').modal('show');
            }
            else{
                $('#name-panelis-send-push').html("");
                var fuente = $('#alert-push-masivo').html();  
                var plantilla = Handlebars.compile(fuente);
                var datos = {  
                    valor: "push-alert"
                };
                var resultadoFinal = plantilla(datos);
                $('#form_panelist_send_push').html(resultadoFinal);
                $('#modal-panelis-send-push').modal('show');
            }

            return false;
        })
        .on('click', '.panelist-sent-push', function (event) {
            event.preventDefault();
            var fuente = $('#form-push-masivo').html();  
            var plantilla = Handlebars.compile(fuente);
            var datos = {  
                valor: "push-simple"
            };
            var resultadoFinal = plantilla(datos);
            $('#form_panelist_send_push').html(resultadoFinal);
        })
    ;

});

var dataDatatableConfig = {
        "dom": 'lfBrtip',
        "buttons": [
            {
                "extend": 'print',
                "text": '<span class="fa fa-print"></span>',
                "orientation": "landscape",
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8 ],
                },
                "title": 'Listado de Panelistas',
                "titleAttr": 'IMPRIMIR',
            },
            {
                "extend": 'excelHtml5',
                "text": '<span class="fa fa-file-excel-o"></span>',
                "orientation": "landscape",
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8 ]
                },
                "title": 'Listado de Panelistas',
                "titleAttr": 'EXCEL'
            },
            {
                "extend": 'pdfHtml5',
                "text": '<span class="fa fa-file-pdf-o"></span>',
                "orientation": "landscape",
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8 ]
                },
                "title": 'Listado de Panelistas',
                "titleAttr": 'PDF'
            },
            {
                "text": '<span class="glyphicon glyphicon-bullhorn"></span>',
                "className": 'push-masive-button',
                "titleAttr": 'Envío de Push/SMS Masivo'
            }
        ],
        "columnDefs": [
            {
                "title": "<input type=\"checkbox\" class=\"panelist_checkbox\" id=\"panelist_checkbox\" value=\"1\">",
                "targets": 0,
                "className": 'text-center panelist_all',
                "orderable": false
            }
        ]
};

function enviarPush (inicio, enviadas){
    var panelistas = document.getElementById('panelistas').value.split(",");
    var total = panelistas.length - 1;
    $('#panelista').val(panelistas[inicio]);
    $.ajax({
                async: true,
                data: $('#form_panelist_send_push').serialize(),
                url:  "/panelista/notify/send_push",
                type:  'POST',
                beforeSend: function () {
                    $('#form_panelist_send_push').find('input, textarea, button, select, radio').attr('disabled','disabled');
                    $('#respuesta').html("<i class='fa fa-clock-o'></i>");
                },
                success:  function (response) {
                    $('#form_panelist_send_push').find('input, textarea, button, select, radio').removeAttr("disabled");
                    $('#respuesta').html('');
                    inicio = inicio + 1;
                    enviadas = enviadas + 1;
                    if(enviadas < total){
                        enviarPush (inicio, enviadas);
                    }
                    if(enviadas == total){
                        if(total == 1){
                            if(response.result == 1){
                                notificar.success(response.message);
                            }
                            if(response.result == 0){
                                notificar.error(response.message);
                            }
                        }
                        else{
                            notificar.success("Se ha enviado la notificación exitosamente");
                        }
                       
                        simulateClick('#cerrar_modal');
                    }
                }
    });
}

$().ready(function () {
    $("#form_panelist_send_push").validate({
        debug: true,
        rules: {
            tipo: { required: true },
            mensaje: { required: true, maxlength: 110 }
        },
        messages: {
            mensaje: {
                required: "Debe ingresar el mensaje de la notificación",
                maxlength: "El mensaje no puede tener más de 110 caracteres"
            }
        },
        errorElement: "span",
        submitHandler: function(form) {
            event.preventDefault();
            enviarPush (0, 0);
        }
    });
});



$('body')
    .on('click', '#panelist_checkbox', function (event) {
        $(".panelist_checkbox").each(function(){
            $(this).prop('checked',$('#panelist_checkbox').prop("checked"));
            if($('#panelist_checkbox').prop("checked") == true){
                $(this).parent('td').parent('tr').addClass("selected");
            }else{
                $(this).parent('td').parent('tr').removeClass("selected");
            } 
        });
    })
;