$(function() {

    $('#fecha_registro_button').daterangepicker(
        {
          locale: {
            "format": 'DD/MM/YYYY',
            "customRangeLabel": "Rango de fechas",
          },
          ranges: {
            'Hoy': [moment(), moment()],
          },
          dateLimit: {
            days: 365
          },
          opens: "right",
          minDate: moment().subtract(365, 'days'),
          maxDate: moment(),
          startDate: moment().subtract(2, 'days'),
          endDate: moment()
        },
        function (start, end, label) {
          $('#fecha_registro_button span').html(label);
          $('#fecha_registro').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        }
    );

    $('#fecha_sincronizacion_button').daterangepicker(
        {
          locale: {
            "format": 'DD/MM/YYYY',
            "customRangeLabel": "Rango de fechas",
          },
          ranges: {
            'Hoy': [moment(), moment()],
          },
          dateLimit: {
            days: 365
          },
          opens: "left",
          minDate: moment().subtract(365, 'days'),
          maxDate: moment(),
          startDate: moment(),
          endDate: moment(),
        },
        function (start, end, label) {
          $('#fecha_sincronizacion_button span').html(label);
          $('#fecha_sincronizacion').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        }
    );

});