(function($){
    var dataTableDefaultOptions = {
       "paging": true,
       "lengthChange": true,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ Elementos",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "No se encontraron resultados",
            "sInfo":           "Mostrando _START_ a _END_ de _TOTAL_",
            "sInfoEmpty":      "Mostrando 0 al 0 de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    };
    if (typeof dataDatatableConfig != "undefined"){
        Object.assign( dataTableDefaultOptions, dataDatatableConfig );
    }
    $(function(){

        $('table.table-datatable').DataTable(dataTableDefaultOptions);
        $('body')
            .on('click', '.action-url', function (event) {
                event.preventDefault();
                var $td = $(this), url = $td.parent().data('row-action');
                document.location.replace(url);
            })
        });
    
})(jQuery);
